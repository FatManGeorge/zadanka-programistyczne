﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace WI_DZWON___Szkolne_dzwonki
{
    class SzkolneDzwonki
    {
        static List<String> CzasWejsciowy = new List<string>();
        static List<DateTime> Godziny = new List<DateTime>();

        static void Main(string[] args)
        {
            PobierzDaneWejsciowe();
            WypelnijListeGodziny();
            WypiszElementyListyGodziny();
        }

        private static void PobierzDaneWejsciowe()
        {
            while (true)
            {
                string Wczytaj = Console.ReadLine();

                if (String.IsNullOrEmpty(Wczytaj))
                {
                    break;
                }
                else
                {
                    CzasWejsciowy.Add(Wczytaj);
                }
            }
        }

        private static void WypiszElementyListyGodziny()
        {
            for (int i = 0; i < Godziny.Count; i++)
            {
                if ( i == Godziny.Count -1)
                {
                    Console.WriteLine(Godziny[i].ToShortTimeString());
                }
                else
                {
                    Console.Write(Godziny[i].ToShortTimeString() + ",");
                }
            }
        }

        private static void WypelnijListeGodziny()
        {
            for (int i = 0; i < CzasWejsciowy.Count; i++)
            {
                if(i == 0)
                {
                    Godziny.Add(DateTime.ParseExact(CzasWejsciowy[i], "HH:mm", CultureInfo.InvariantCulture));
                }
                else
                {
                    Godziny.Add(Godziny[Godziny.Count-1].AddMinutes(45));
                    Godziny.Add(Godziny[Godziny.Count - 1].AddMinutes(Convert.ToDouble(CzasWejsciowy[i])));
                }
            }
        }
    }
}
