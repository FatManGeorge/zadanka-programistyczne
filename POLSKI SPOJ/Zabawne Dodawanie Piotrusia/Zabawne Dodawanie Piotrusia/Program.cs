﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zabawne_Dodawanie_Piotrusia
{
    class Program
    {
        static int taskID = 568;

        static void Main(string[] args)
        {
            int iloscZestawow = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < iloscZestawow; i++)
            {
                string liczbaDoSprawdzenia = Console.ReadLine();

                int tries = 0;

                while(!(SprawdzPoprawnosc(liczbaDoSprawdzenia, Odwroc(liczbaDoSprawdzenia))))
                {
                    liczbaDoSprawdzenia = ZwrocLiczbePolaczona(liczbaDoSprawdzenia, Odwroc(liczbaDoSprawdzenia));
                    tries++;
                }

                Console.WriteLine(liczbaDoSprawdzenia + " " + tries);
            }

        }
        
        static string Odwroc(String s)
        {
            char[] stringDoOdwrocenia = s.ToCharArray();
            Array.Reverse(stringDoOdwrocenia);
            return new string(stringDoOdwrocenia);
        }

        static bool SprawdzPoprawnosc(string Normalny, String odwrocony)
        {
            return Normalny == odwrocony ? true : false;
        }

        static string ZwrocLiczbePolaczona(string Normalny, string odwrocony)
        {
            int liczbaOdwrocona = Convert.ToInt32(Normalny) + Convert.ToInt32(odwrocony);
            string odwroconaLiczba = liczbaOdwrocona.ToString();
            return odwroconaLiczba;
        }
    }
}
