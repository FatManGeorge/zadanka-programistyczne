﻿using System;

namespace Tablica
{
    public class Program
    {
        private const int ID = 977;

        private static string[] wejscie;

        static void Main(string[] args)
        {
            wejscie = Console.ReadLine().Split(' ');

            for (int i = wejscie.Length-1; i >= 0; i--)
            {
                Console.Write(wejscie[i]+" ");
            }
        }
    }
}
