﻿using System;
using System.Collections.Generic;

namespace SUMA
{
    class Program
    {
        /// <summary>
        /// Id testu na SPOJ
        /// </summary>
        public static int TaskID = 968;

        static void Main(string[] args)
        {
            List<int> liczbyNaWyjsciu = new List<int>();

            int liczbyNaWejsciu = 0;

            while(true)
            {
                string liczbaWejsciowa = Console.ReadLine();

                if (String.IsNullOrEmpty(liczbaWejsciowa))
                {
                    break;
                }
                else
                {
                    liczbyNaWejsciu += Convert.ToInt32(liczbaWejsciowa);
                    liczbyNaWyjsciu.Add(liczbyNaWejsciu);
                }
                
            }

            foreach (var liczba in liczbyNaWyjsciu)
            {
                Console.WriteLine(liczba);
            }
        }
    }
}
