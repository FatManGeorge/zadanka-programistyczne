﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mniejsze_niż
{
    class Program
    {
        static int taskID = 783;

        public static int[] zbior { get; set; }

        static void Main(string[] args)
        {
            zbior = new int[WprowadzRozmiarZbioru()];
            WypelnijZbior(zbior);
            Console.WriteLine();

            int iloscTestow = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < iloscTestow; i++)
            {

                Console.WriteLine(PorownajIleJestMniejszychElementow(Convert.ToInt32(Console.ReadLine()), zbior));
            }

        }

        private static void WyswietlZawartoscZbioru(int[] zbior)
        {
            for (int i = 0; i < zbior.Length; i++)
            {
                Console.WriteLine(zbior[i]);
            }
        }

        private static int WprowadzRozmiarZbioru()
        {
            return Convert.ToInt32(Console.ReadLine());
        }

        static int[] WypelnijZbior(int[] zbior)
        {
            for (int i = 0; i < zbior.Length; i++)
            {
                zbior[i] = Convert.ToInt32(Console.ReadLine());
            }
            return Program.zbior;
        }
        
        static int PorownajIleJestMniejszychElementow(int test, int[] zbior)
        {
            int mniejszychElementow = 0;
            for (int i = 0; i < zbior.Length; i++)
            {
                if(test > zbior[i])
                {
                    mniejszychElementow++;
                }
            }
            return mniejszychElementow;
        }
        
    }
}
