﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DOUGHNUT___Harry_and_big_doughnuts
{
    class Program
    {
        private int ID = 4138;

        private static int[] dane = new int[3];

        static void Main(string[] args)
        {
            int iloscTestow = Int32.Parse(Console.ReadLine());

            for (int i = 0; i < iloscTestow; i++)
            {
                WczytajDane();
                SprawdzDaneIWypiszWynik(dane);
            }
        }

        private static void WczytajDane()
        {
            string[] wejscie = Console.ReadLine().Split(' ');

            for (int i = 0; i < 3; i++)
            {
                Program.dane[i] = Int32.Parse(wejscie[i]);
            }
        }

        private static void SprawdzDaneIWypiszWynik(int[] dane)
        {
            int ciezar = dane[0] * dane[2];
            if(ciezar<= dane[1])
            {
                Console.WriteLine("yes");
            }
            else
            {
                Console.WriteLine("no");
            }
        }
    }
}
