﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tabelki_Liczb
{
    class Program
    {
        public const int ID = 1056;

        String nazwaWyzwania = "Tabelki liczb";

        public static int columns { get; set; }

        public static int lines { get; set; }

        static void Main(string[] args)
        {
            Int32.TryParse(Console.ReadLine(), out int numberOfTestLoops);
            for (int i = 0; i < numberOfTestLoops; i++)
            {
                String[] rozmiar = Console.ReadLine().Split(' ');
                lines = Convert.ToInt32(rozmiar[0]);
                columns = Convert.ToInt32(rozmiar[1]);

                char[,] ukladankaLiczbowa = new char[lines, columns];
                for (int y = 0; y < lines; y++)
                {
                    String[] liniaWejsciowa = Console.ReadLine().Split(' ');
                    for (int z = 0; z < columns; z++)
                    {
                        ukladankaLiczbowa[y, z] = Convert.ToChar(liniaWejsciowa[z]);
                    }
                }
                char[,] obroconaTablica = new char[lines, columns];
                ObrocTabliceZnakow(ukladankaLiczbowa,obroconaTablica);
                WyswietlTabliceZnakow(obroconaTablica);
            }
            Console.WriteLine();
        }

        public static void WyswietlTabliceZnakow(char[,] tablica)
        {
            for (int i = 0; i < lines ; i++)
            {
                for (int y = 0; y <columns ; y++)
                {
                    if(y-1 == columns )
                    {
                        Console.Write(tablica[i, y]);
                    }
                    else
                    { 
                        Console.Write(tablica[i, y]+" ");
                    }
                }
                Console.WriteLine();
            }
        }
        
        public static void ObrocTabliceZnakow(char[,] tablica, char[,] DocelowaTablica)
        {
            for (int i = 0; i < lines; i++)
            {
                for (int y = 0; y < columns; y++)
                {
                    if(y == 0)
                    {
                        if(i == lines-1)
                        {
                            DocelowaTablica[i, y + 1] = tablica[i, y];
                        }
                        else
                        {
                            DocelowaTablica[i,y] = tablica[i+1, y];
                        }
                    }
                    else
                    {
                        DocelowaTablica[i, y] = tablica[i, y - 1];
                    }
                }
            }
        }
    }
}
