﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PROGC05___Wycinanie_literek
{
    class Program
    {
        private int id = 2181;

        static void Main(string[] args)
        {
            string wejscie = " ";
            while (!String.IsNullOrEmpty(wejscie))
            {
                wejscie = Console.ReadLine();
                if(!string.IsNullOrEmpty(wejscie))
                    Console.WriteLine(WyfiltorwanyCiag(wejscie));
            }
        }

        private static string WyfiltorwanyCiag(string wejscie)
        {
                string result = string.Empty;
                string[] wejscieBezSpacji = wejscie.Split(' ');
                string lancuchDoFiltrowania = wejscieBezSpacji[1];
                char[] znakOmijany = wejscieBezSpacji[0].ToCharArray();

                for (int i = 0; i < lancuchDoFiltrowania.Length; i++)
                {
                    if (znakOmijany[0] != lancuchDoFiltrowania[i])
                    {
                        result += lancuchDoFiltrowania[i];
                    }
                }
                return result;
        }
    }
}
